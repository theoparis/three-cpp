#include "shader.h"
#include <stdexcept>

namespace three {
    Shader::Shader(std::string &source, unsigned int type) {
        id = glCreateShader(type);

        const char *cSource = source.c_str();
        glShaderSource(id, 1, &cSource, nullptr);
        glCompileShader(id);

        int result;
        glGetShaderiv(id, GL_COMPILE_STATUS, &result);

        if (result == GL_FALSE) {
            int length;
            glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
            char *message = (char *)alloca(length * sizeof(char));
            glGetShaderInfoLog(id, length, &length, message);

            std::string msg = "Failed to compile";
            if (type == GL_VERTEX_SHADER)
                msg += " vertex shader ";
            else
                msg += " fragment shader ";
            msg += std::string(message);

            glDeleteShader(id);
            throw std::runtime_error(msg);
        }
    }

    auto Shader::getId() -> unsigned int { return id; }

    ShaderProgram::ShaderProgram(unsigned int vertexShader,
                                 unsigned int fragmentShader) {
        this->id = glCreateProgram();

        glAttachShader(id, vertexShader);
        glAttachShader(id, fragmentShader);
        glLinkProgram(id);
        glValidateProgram(id);

        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
    }

    void ShaderProgram::bind() { glUseProgram(id); }

    void ShaderProgram::setUniform(std::string location, glm::mat4 data) {
        glUniformMatrix4fv(glGetUniformLocation(id, location.c_str()), 1,
                           GL_FALSE, &data[0][0]);
    }

    void ShaderProgram::unbind() { glUseProgram(0); }
} // namespace three

#include "mesh.h"

#include "glad/glad.h"
#include "GLFW/glfw3.h"

namespace three {
    Mesh::Mesh(std::vector<float> vertices)
        : vao(-1), vbo(-1), vertices(vertices) {
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        glGenBuffers(1, &vbo);
        update();

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3,
                              nullptr);
        glEnableVertexAttribArray(0);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    void Mesh::draw() {
        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLES, 0, vertices.size() / 3);
    }

    void Mesh::update() {
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float),
                     &vertices[0], GL_STATIC_DRAW);
    }
} // namespace three

#pragma once
#include <glm/glm.hpp>
#include <iostream>
#include <string>
#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include <functional>

namespace three {
    class App {
        public:
            App(int windowWidth, int windowHeight, std::string programName);

            int run(std::function<void(GLFWwindow *)> update);

            glm::vec2 getWindowSize();

        private:
            static void framebuffer_size_callback(GLFWwindow *window, int width,
                                                  int height);

            void teardown(GLFWwindow *window);

            int windowWidth;
            int windowHeight;
            std::string programName;

            GLFWwindow *window;
    };
} // namespace three

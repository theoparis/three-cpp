#pragma once
#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include <string>
#include "glm/glm.hpp"

namespace three {
    class Shader {
        public:
            Shader(std::string &source, unsigned int type);

            auto getId() -> unsigned int;

        private:
            unsigned int id;
    };

    class ShaderProgram {
        public:
            ShaderProgram(unsigned int vertexShader,
                          unsigned int fragmentShader);

            void bind();
            void unbind();

            void setUniform(std::string location, glm::mat4 data);

        private:
            unsigned int id;
    };
} // namespace three
